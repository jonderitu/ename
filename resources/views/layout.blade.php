<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Development Company" content="Cytonn Technologies">
    <meta name="Developer" content="Joseph Gichane">
    <title>ENAME Cytonn App</title>
    <link rel="shortcut icon" href="/site_images/favicon.ico" type="image/x-icon"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>

<div data-sticky-container>
    <div data-sticky data-margin-top='0' data-top-anchor="header:bottom" data-btm-anchor="content:bottom">
        <div class="top-bar">
            <div class="top-bar-title">
                <img class=".login-container-img" src="/site_images/logo.png" >
            </div>
            <div class="top-bar-right">
                <ul class="menu menu-green">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Propose name </a></li>
                    <li><a href="#">Vote</a></li>
                    <li><a href="#">polls</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


@yield('content')

</body>
</html>

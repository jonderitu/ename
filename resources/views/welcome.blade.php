<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="Development Company" content="Cytonn Technologies">
        <meta name="Developer" content="Joseph Gichane">
        <title>ENAME Cytonn App</title>
        <link rel="shortcut icon" href="/site_images/favicon.ico" type="image/x-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet">
    </head>
    <body>
    <div class="login">
        <div class="row">
            <div class="medium-6 medium-centered large-4 large-centered columns">
            <div class="login-container">
                    <div class="login-container-img">
                        <img src="/site_images/ct_logo.png"/>
                    </div>

                    <div class="login-container-text">
                        <p>Login with your corporate email</p>

                        <div></div><p><a class="large-6 button green" href="#">Login with google</a></div>
                <p>Login with your  email</p>
                        <div></div><a class="large-6 button green" href="/auth/login">Login</a></div></p>
                <p>Registe for new account</p>
                <div></div><a class="large-6 button green" href="/auth/register">Register</a></div></p>
                        <p>&copy;Cytonn 2016 All Right Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </body>
</html>
